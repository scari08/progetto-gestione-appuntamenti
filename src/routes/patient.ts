import { Router } from "express";
import { createPatient, deletePatient, getAllPatients, getPatient, updatePatient } from "../controllers/patient";

/* :id temporaneamente sarà l'ObjectId di mongoDb. */

const router = Router();

router.post("/", createPatient);

router.get("/", getAllPatients);

router.get("/:id", getPatient);

router.patch("/:id", updatePatient);

router.delete("/:id", deletePatient);

export default router;
