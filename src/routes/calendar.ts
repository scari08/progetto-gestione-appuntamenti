import { Router } from "express";
import { getCalendar } from "../controllers/calendar";

const router = Router();

router.get("/", getCalendar)

export default router;