import { json } from "body-parser";
import express, { NextFunction, Request, Response } from "express";
import patientRoutes from "./routes/patient";
import calendarRoutes from "./routes/calendar";
import { connectToDatabase } from "./services/dbservice";

const app = express();
connectToDatabase().then(() => {
  app.use(json());
  
  app.use((req, res, next) => {
    // Set CORS headers so that the React SPA is able to communicate with this server
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'GET,POST,PUT,PATCH,DELETE,OPTIONS'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });

  app.use("/patient", patientRoutes);
  app.use("/calendar", calendarRoutes);
  app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).json({ message: err.message });
  });
  app.listen(3000);
  
});
