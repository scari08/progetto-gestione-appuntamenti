import { RequestHandler } from "express";
import { db } from "../services/dbservice";

export const getCalendar: RequestHandler = (req, res, next) => {
  //Richiesta GET in formato json delle date ordinate dall'array di appuntamenti
  const appointments: Object[] = [];
  db.patient
    ?.aggregate([{ $unwind: "$appointments" }, { $project: { _id: 0, "appointments.date": 1 } }, { $sort: { "appointments.date": 1 } }])
    .forEach((element) => {
      console.log(element);
      appointments.push(element);
    })
    .then(() => res.status(200).json({ ...appointments }))
    .catch((error) => console.log(error));
};

export async function test() {
  const appointments: Object[] = [];
  await db.patient
    ?.aggregate([{ $unwind: "$appointments" }, { $project: { _id: 0, "appointments.date": 1 } }, { $sort: { "appointments.date": 1 } }])
    .forEach((element) => {
      console.log(element);
      appointments.push(element);
    })
    .then(() => {return appointments})
    .catch((error) => console.log(error));
};
