import { RequestHandler } from "express";
import { ObjectId } from "mongodb";
import Patient from "../models/patient";
import { db } from "../services/dbservice";

/*  Operazioni CRUD in REST API */

export const createPatient: RequestHandler = (req, res, next) => {
  //Info prese da form in front
  const name = (req.body as { name: string }).name;
  const appointments: { docName: string; date: Date }[] = [{ docName: (req.body as { docName: string }).docName, date: new Date(req.body.date) }];
  const newPatient = new Patient(name, appointments);

  db.patient
    ?.insertOne(newPatient)
    .then((result) => res.status(201).json({ message: "Il paziente è stato creato nel DB!", createdPatientId: result.insertedId }))
    .catch((error) => next(error));
};

export const getAllPatients: RequestHandler = (req, res, next) => {
  //Temporanea risposta fetch tutti i pazienti in formato json
  const patients: Patient[] = [];
  db.patient
    ?.find<Patient>({})
    .forEach((element) => {
      patients.push(element);
    })
    .then(() => res.status(200).send(JSON.stringify(patients)))
    .catch((error) => console.log(error));
};

export const getPatient: RequestHandler<{ id: string }> = (req, res, next) => {
  //Restituisce le informazioni del paziente interrogato con _id
  const patientId = req.params.id;
  db.patient
    ?.findOne<Patient>({ _id: new ObjectId(patientId) })
    .then((result) => res.status(200).json({ message: result?.name }))
    .catch((error) => console.log(error));
};

export const updatePatient: RequestHandler<{ id: string }> = (req, res, next) => {
  //Info prese da form in front
  const patientId = req.params.id;
  const docName: string = (req.body as { docName: string }).docName;
  const date: Date = new Date((req.body as { date: string }).date);
  db.patient?.updateOne({ _id: new ObjectId(patientId) }, { $push: { appointments: { docName: docName, date: date } } }).then((result) => {
    res.json({ message: `update ${result.acknowledged}` });
  });
};

export const deletePatient: RequestHandler<{ id: string }> = (req, res, next) => {
  //Cancella il paziente con query _id dal database
  const patientId = req.params.id;
  db.patient?.deleteOne({ _id: new ObjectId(patientId) }).then((result) => {
    res.json({ message: `DELETED ${result.acknowledged}` });
  });
};
