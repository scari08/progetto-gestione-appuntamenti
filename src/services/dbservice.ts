/* Funzioni di connessione al database. */
import { MongoClient, Collection } from "mongodb";

export const db: { patient?: Collection } = {};

// Possono essere sostituite qualunque di queste stringhe per db con nomi di collezioni diversi
const uri: string = "mongodb://0.0.0.0:27017/?readPreference=primary&ssl=false";
const dbName: string = "dentist";
const collectionName: string = "patient";
const client: MongoClient = new MongoClient(uri);

export async function connectToDatabase() {
  await client.connect();
  if (!client) {
    console.log("Connessione al database fallita!");
    return;
  }
  console.log("Connessione al database stabilita!");
  const clientDb = client.db(dbName);
  db.patient = clientDb.collection(collectionName);
  db.patient.indexExists("appointments.docName_1_appointments.date_1").then((res) => (res ? null : db.patient?.createIndex({ "appointments.docName": 1, "appointments.date": 1 }, { unique: true })));
}

export async function closeDatabase() {
  await client.close();
  console.log("Connessione al database chiusa!");
}
