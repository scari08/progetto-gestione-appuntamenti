import { Route, Routes } from "react-router-dom";
import React, { useState } from "react";
import AllPatients from "./pages/AllPatients";
import MainNavigation from "./components/layouts/MainNavigation";
import NewPatient from "./pages/NewPatient";
import axios from "axios";

const api = axios.create({ baseURL: "http://localhost:3000/patient" });
const dbpatients: any[] = [];

api.get("/").then((res) => {
  const temp = res.data;
  temp.map((elem: any) => dbpatients.push(elem));
});

// function newFunction() {
//   api.get("/").then((res) => {
//     const temp = res.data;
//     temp.map((elem: any) => dbpatients.push(elem));
//   });
// }

const App: React.FC = () => {
  const [patients, setPatients] = useState<{}[]>(dbpatients);

  const newPatientsHandler = (newPatients: any[]) => {
    setPatients((prevPatients) => (prevPatients = newPatients));
  };

  const patientDeleteHandler = (_id: string) => {
    api.delete("/" + _id).then(() => {
      const a: any[] = [];
      api.get("/").then((res) => {
        const temp = res.data;
        temp.map((elem: any) => a.push(elem));
        setPatients(a);
      });
    });
  };

  const patientUpdateHandler = (_id: string, docName: string, date: Date) => {
    api.patch("/" + _id).then(() => {
      const a: any[] = [];
      api.get("/").then((res) => {
        const temp = res.data;
        temp.map((elem: any) => a.push(elem));
        setPatients(a);
      });
    })
  }

  return (
    <div className="App">
      <MainNavigation />
      <Routes>
        <Route path="/patients" element={<AllPatients patients={patients} onPatientDelete={patientDeleteHandler} />}></Route>
        <Route path="/newpatient" element={<NewPatient onNewPatient={newPatientsHandler} />}></Route>
      </Routes>
    </div>
  );
};

export default App;
