import axios from "axios";
import React, { useRef } from "react";

const api = axios.create({ baseURL: "http://localhost:3000/patient" });
type NewPatientProps = {
  onNewPatient: (newPatients: any[]) => void;
};

const NewPatient: React.FC<NewPatientProps> = (props) => {
  const nameInputRef = useRef<HTMLInputElement>(null);
  const docNameInputRef = useRef<HTMLInputElement>(null);
  const dateInputRef = useRef<HTMLInputElement>(null);

  const [errorMessage, setErrorMessage] = React.useState("");

  const newPatientHandler = (event: React.FormEvent) => {
    event.preventDefault();
    const enteredName = nameInputRef.current!.value;
    const enteredDocName = docNameInputRef.current!.value;
    const enteredDate = dateInputRef.current!.value;
    const tempDate = new Date(enteredDate);
    api
      .post("/", { name: enteredName, docName: enteredDocName, date: tempDate })
      .then(() => {
        const newPatients: any[] = [];
        api.get("/").then((res) => {
          const temp = res.data;
          temp.map((elem: any) => newPatients.push(elem));
          props.onNewPatient(newPatients);
        });
        setErrorMessage("Paziente Aggiunto con successo!");
      })
      .catch((res) => {
        if (res.response.data.message.includes("E11000 duplicate key error collection")) {
          setErrorMessage("Orario già occupato!");
        } else {
          setErrorMessage(res.response.data.message);
        }
      });
  };

  return (
    <form onSubmit={newPatientHandler}>
      <div>
        <label htmlFor="name-text">Nome Paziente</label>
        <input type="text" id="name-text" ref={nameInputRef} />
      </div>
      <div>
        <label htmlFor="docName-text">Nome Dottore</label>
        <input type="text" id="docName-text" ref={docNameInputRef} />
      </div>
      <div>
        <label htmlFor="date-text">Data Appuntamento</label>
        <input type="date" id="date-text" ref={dateInputRef} />
      </div>
      <button type="submit">AGGIUNGI PAZIENTE</button>
      {errorMessage && <div className="error"> {errorMessage} </div>}
    </form>
  );
};

export default NewPatient;