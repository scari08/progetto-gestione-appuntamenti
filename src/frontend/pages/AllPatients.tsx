import PatientList from "../components/patient/PatientList";


function AllPatients(props: { patients: any[], onPatientDelete: (_id: string) => void }) {
  return (
    <section>
      <h1>Lista completa appuntamenti</h1>
      <PatientList patients={props.patients} onPatientDelete={props.onPatientDelete} />
    </section>
  );
}

export default AllPatients;