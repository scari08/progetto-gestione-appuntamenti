import { Link } from "react-router-dom";
import classes from "./MainNavigation.module.css";

const MainNavigation = () => {
  return (
    <header className={classes.header}>
      <div className={classes.logo}>PAGINA PRINCIPALE</div>
      <nav>
        <ul>
          <li>
            <Link to={"/"}>HOME</Link>
          </li>
          <li>
            <Link to={"/newPatient"}>AGGIUNGI PAZIENTE</Link>
          </li>
          <li>
            <Link to={"/patients"}>LISTA APPUNTAMENTI</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;