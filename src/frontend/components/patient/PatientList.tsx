import PatientItem from "./PatientItem";

function PatientList(props: { patients: any[]; onPatientDelete: (_id: string) => void }) {
  return (
    <ul>
      {props.patients.map((patient) => (
        <PatientItem key={patient._id.toString()} _id={patient._id} name={patient.name} appointments={patient.appointments} onPatientDelete={props.onPatientDelete} />
      ))}
    </ul>
  );
}

export default PatientList;
