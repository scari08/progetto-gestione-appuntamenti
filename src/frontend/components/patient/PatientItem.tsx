function PatientItem(props: { _id: string; name: string; appointments: { docName: string; date: Date }[], onPatientDelete: (_id: string) => void }) {
  return (
    <li key={props._id.toString()}>
      <div>
        <h3>{props.name} <button onClick={props.onPatientDelete.bind(null, props._id.toString())}>ELIMINA</button></h3>
        
        <div>
          {props.appointments.map((appointment) => {
            return (
              <div key={appointment.date.toString()}>
                <p>
                  Appuntamento con <b>{appointment.docName}</b> il giorno: {appointment.date}
                </p>
                <button>AGGIUNGI APPUNTAMENTO</button>
              </div>
            );
          })}
        </div>
      </div>
    </li>
  );
}

export default PatientItem;
