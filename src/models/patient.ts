/* Schema base della collezione di pazienti.
Per semplificazione in fase iniziale sono stati usati campi provvisori
*/
import { ObjectId } from "mongodb";

export default class Patient {
  public constructor(
    public name: string,
    public appointments: { docName: string; date: Date }[], //nel db {appointments.docName, appointments.date} sono indicizzati
    public _id?: ObjectId
  ) {}
}
